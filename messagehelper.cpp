#include "messagehelper.h"

void MessageHelper::log(const std::string &message){
    std::cout << message << "\n";
}

void MessageHelper::printLines(int lines)
{
    for (int i=0;i<lines;i++){
        std::cout << "\n";
    }
};