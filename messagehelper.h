#ifndef MESSAGEHELPER_H
#define MESSAGEHELPER_H


#include <string>
#include <iostream>
class MessageHelper{
public:
    static void log(const std::string &message);
    static void printLines(int i);
};

#endif // MESSAGEHELPER_H