binary: main.o libMessage.a
	g++ -o binary main.o -L. -lMessage

main.o: main.cpp
	g++ -c main.cpp

libMessage.a: messagehelper.o
	ar cr libMessage.a messagehelper.o

messagehelper.o: messagehelper.cpp
	g++ -c messagehelper.cpp

clean:
	rm -f *.o *.a binary